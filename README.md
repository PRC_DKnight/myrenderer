# Clustered Forward/Deferred Rendering Engine 

### How To Run
With Windows X64 MinGW compilation, you should be able to run this project without any problems. Other platforms need to compile their own dependent libraries.

### Main Features

* Clustered Forward/Deferred Renderer
* Physically Based shading
* Image Based Lighting
* Metallic workflow
* Cook-Torrance specular BRDF 
* Ambient Occlusion & Emissive mapping
* Tangent space normal mapping
* HDR/linear lighting
* HDR/LDR skyboxes
* Exposure based tone mapping
* Bloom
* Multisample Anti-aliasing (MSAA)
* Directional & point light sources
* Compute shader based light culling
* Directional light dynamic shadow mapping
* Static Omnidirectional Shadow Mapping for Point Lights


TODO：
* BVH
* Optimization and compression of G-buffer
* FXAA
* SSAO
* Vulkan/DX support
* RayTracing
* Upgrade to a game engine?

## Render Samples
![helmetcrop](./assets/sample/1.png)

![waterbottlefixed](./assets/sample/2.png)

![metallicspheres](./assets/sample/4.png)

![sponza](./assets/sample/3.png)

![G-Buffer](./assets/sample/11.png)

### Models & Textures

* [Sponza (gLTF2 Version)](https://github.com/KhronosGroup/glTF-Sample-Models/tree/master/2.0/Sponza)
  : Original model: [Frank Meinl](https://www.artstation.com/artwork/K5bEr), First modified by: [Morgan McGuire](http://casual-effects.com/data/index.html), PBR Textures: [Alexandre-pestana](http://www.alexandre-pestana.com/pbr-textures-sponza/).
* [Barcelona Sunrise (HDR Map)](http://www.hdrlabs.com/sibl/archive.html): by [@Blochi](https://twitter.com/Blochi)
* [Battle Damaged Sci-fi Helmet (PBR)](https://github.com/KhronosGroup/glTF-Sample-Models/tree/master/2.0/DamagedHelmet): by [@theblueturtle_](https://sketchfab.com/theblueturtle_)
* [Metal Rough Test Spheres (PBR)](https://github.com/KhronosGroup/glTF-Sample-Models/tree/master/2.0/MetalRoughSpheres): by Ed Mackey.
* [gLTF Sample Model Repository](https://github.com/KhronosGroup/glTF-Sample-Models): All other models & textures.
* [sIBL Archive](http://www.hdrlabs.com/sibl/archive.html): All other HDR maps & skyboxes.
* [Default Skybox](http://www.custommapmakers.org/skyboxes.php)

## References

* [Hybrid Rendering Engine](https://github.com/Angelo1211/HybridRenderingEngine): HRE was a real-time, Physically based, Clustered renderer built with OpenGL and based on the techniques described on the paper Clustered Deferred and Forward shading by Ola Olsson, Markus Billeter and Ulf Assarsson.

* [Learn OpenGL](https://learnopengl.com/Introduction): OpenGL tutorials and general introduction to 3D graphics concepts.
* [Parallel Computer Architecture and Programming](http://15418.courses.cs.cmu.edu/tsinghua2017/home) : Intro to the GPU programming model.
* [Doom(2016) - Graphics Study](http://www.adriancourreges.com/blog/2016/09/09/doom-2016-graphics-study/): Dissection of a frame in the id Tech 6 engine.
* [Siggraph2016 - The Devil is in the Details: idTech 666](https://www.slideshare.net/TiagoAlexSousa/siggraph2016-the-devil-is-in-the-details-idtech-666?next_slideshow=1): A behind-the scenes look into the renderer tech of DOOM 2016. Probably the biggest inspiration in terms of features and overall quality level that I'm striving for.
* [Efficient Real-Time Shading with Many Lights](https://www.zora.uzh.ch/id/eprint/107598/1/a11-olsson.pdf): An introduction to clustered shading directly from the writers of the original paper. Also outlines some practical tips from industry veterans.
* [Real Shading in Unreal Engine 4](https://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf): The principles and implementation details behind UE4 physically based shading model. Includes shader code!
* [Forward vs Deferred vs Forward+ Rendering with DirectX 11](https://www.3dgep.com/forward-plus/): Detailed overview of different rendering algorithms.
