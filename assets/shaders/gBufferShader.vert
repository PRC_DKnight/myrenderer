#version 450 core
//Naming scheme clarification
// ms = model Space
// vs = view Space
// ws = world Space
// ts = tangent Space
// ts = tangent Space

layout (location = 0) in vec3 vertexPos_ms;
layout (location = 1) in vec3 normal_ms;
layout (location = 2) in vec2 aTexCoord;
layout (location = 3) in vec3 tangent_ts;
layout (location = 4) in vec3 biTangent_ts;


out VS_OUT{
    vec4 fragPos_ws;
    vec2 texCoords;
    mat3 TBN;
} vs_out;

uniform mat4 MVP;
uniform mat4 M;

void main(){
    //Position in clip space
    gl_Position = MVP*vec4(vertexPos_ms, 1.0);

    //Passing texture coords
    vs_out.texCoords = aTexCoord;

    //World Space fragment position
    vs_out.fragPos_ws    =  M * vec4(vertexPos_ms, 1.0);

    //Generating tangent matrix
    vec3 T = normalize(mat3(M) * tangent_ts);
    vec3 N = normalize(mat3(M) * normal_ms);
    T = normalize(T - dot(T, N ) * N);
    vec3 B = normalize(mat3(M) * biTangent_ts);

    vs_out.TBN = mat3(T, B, N);
}