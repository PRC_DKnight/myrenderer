#version 450 core
//Naming scheme clarification
// mS = model Space
// vS = view Space
// wS = world Space
// tS = tangent Space

layout(location = 0) out vec4 albedoBuffer;
layout(location = 1) out vec4 positionBuffer;
layout(location = 2) out vec3 normalsBuffer;
layout(location = 3) out vec3 emissiveBuffer;
layout(location = 4) out vec3 lightBuffer;//TODO:Now r for float ao,g for IBL switch(0.4 for true,0.6 for false).going to change in the future
layout(location = 5) out vec3 metalRoughBuffer;

in VS_OUT{
    vec4 fragPos_ws;
    vec2 texCoords;
    mat3 TBN;
} fs_in;

//PBR Textures to sample from
uniform sampler2D albedoMap;
uniform sampler2D emissiveMap;
uniform sampler2D normalsMap;
uniform sampler2D lightMap;//All three channels of AO are one value
uniform sampler2D metalRoughMap;//b for metal,g for Rough
uniform sampler2D shadowMap;

uniform bool aoMapped=false;
uniform bool IBL=false;
uniform int modelId=0;

void main(){
    //Storing albedo values in the first three channels of the third texture buffer
    albedoBuffer = texture(albedoMap, fs_in.texCoords);

    //Storing world space position in the first texture buffer
    positionBuffer = fs_in.fragPos_ws;

    //Storing normal in world space in second texture buffer
    vec3 normal   =  texture(normalsMap, fs_in.texCoords).rgb;
    normalsBuffer = fs_in.TBN * normalize(normal * 2.0 - 1.0);

    emissiveBuffer = texture(emissiveMap,fs_in.texCoords).rgb;
    if(aoMapped){
        lightBuffer = texture(lightMap,fs_in.texCoords).rgb;
    }else{
        lightBuffer = vec3(1.0);
    }
    if(IBL){
        lightBuffer.g = 0.4;
    }else{
        lightBuffer.g = 0.6;
    }
    metalRoughBuffer = texture(metalRoughMap,fs_in.texCoords).rgb;
    metalRoughBuffer.r = float(modelId);
}
