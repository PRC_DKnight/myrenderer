#version 430 core
out vec4 FragColor;

in vec3 TexCoords;
in vec3 pos_ws;
uniform samplerCube skybox;
layout(location = 0) out vec4 albedoBuffer;
layout(location = 1) out vec3 positionBuffer;

void main(){
    albedoBuffer = texture(skybox, TexCoords);
    positionBuffer=pos_ws;
}