#pragma once
#include "texture.hpp"
#include "cubeMap.hpp"
#include "shader.hpp"
#include "glm/glm.hpp"
#include <string>
struct Skybox{
    //Has special depth testing requirements
    void draw();

    //Setup functions for skybox mesh (VAO) and textures
    void setup(const std::string &skyboxName, bool isHDR, int resolution);

    //Transforming the equirecangular texture to a cubemap format for rendering
    void fillCubeMapWithTexture(const Shader &buildCubeMapShader);

    unsigned int resolution;

    //Equirectangular map is not rendered, just an intermediate state
    Texture equirectangularMap;
    CubeMap skyBoxCubeMap;
};

void Skybox::draw(){
    //We change the depth function since we set the skybox to a clipspace value of one
    glDepthFunc(GL_LEQUAL);//GL_LEQUAL,如果输入的深度值小于或等于参考值，则通过
    glDepthMask(GL_FALSE);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, skyBoxCubeMap.textureID);
    skyBoxCubeMap.drawCube();

    glDepthMask(GL_TRUE);
    glDepthFunc(GL_LESS);
}

//Two setup paths for HDR and non HDR cubemap
void Skybox::setup(const std::string &skyboxName, bool isHDR, int res){
    std::string skyBoxFolderPath = "../assets/skyboxes/";
    skyBoxFolderPath += skyboxName;
    std::string skyBoxFilePath = skyBoxFolderPath + "/" + skyboxName + ".hdr";

    resolution = res;

    //If the skybox is HDR it will come in as an equirectangular map,
    //We need to load it in and generate the cubemap that will be shown
    if(isHDR){
        equirectangularMap.loadHDRTexture(skyBoxFilePath);
        skyBoxCubeMap.generateCubeMap(res, res, HDR_MAP);
    }
    else{
        skyBoxCubeMap.loadCubeMap(skyBoxFolderPath);
    }
}

//Instead of passing the shader in, we could call this function when skybox is initialized?
void Skybox::fillCubeMapWithTexture(const Shader &buildCubeMapShader){
    skyBoxCubeMap.equiRectangularToCubeMap(equirectangularMap.textureID, resolution, buildCubeMapShader);
}