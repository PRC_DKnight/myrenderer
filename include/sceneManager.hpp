#pragma once
#include "scene.hpp"
class SceneManager{
public:
    //Dummy Constructor / Destructor
    SceneManager();
    ~SceneManager();

    //Initializes and closes all scene related stuff
    bool startUp(std::string currentSceneID= "Sponza");
    void shutDown();

    // Scene switching
    bool switchScene(std::string sceneID);

    // Update current scene
    void update(unsigned int deltaT);

    //Called by the rendermanager to prep the render queue
    Scene* getCurrentScene();

private:
    bool loadScene(std::string sceneID);

    //String could probably be an enum instead, but it's easier this way to build
    //the relative paths if it is a string.
    std::string currentSceneID;
    Scene* currentScene;
};

SceneManager::SceneManager(){}
SceneManager::~SceneManager(){}

//Starts up the scene manager and loads the default scene. If for whatever reason
// the scene could not load any model, or there are none defined it quits early.
//默认是"Sponza"
bool SceneManager::startUp(std::string currentSceneID){
    // currentSceneID = "pbrTest";
    if (!loadScene(currentSceneID)){
        printf("Could not load default sponza scene. No models succesfully loaded!\n");
        return false;
    }
    return true;
}

void SceneManager::shutDown(){
    delete currentScene;
}

//Checks if the scene that you want to load is not the one that is currently loaded.
// If it isn't, then it deletes the current one and loads the new one.
bool SceneManager::switchScene(std::string newSceneID){
    if( newSceneID != currentSceneID ){
        currentSceneID = newSceneID;
        delete currentScene;
        return loadScene(newSceneID);
    }
    else{
        printf("Selected already loaded scene.\n");
        return true;
    }
}

//Misdirection towards the current scene to avoid pointer dangling after scene switching
void SceneManager::update(unsigned int deltaT){
    currentScene->update(deltaT);
}

Scene* SceneManager::getCurrentScene(){
    return currentScene;
}

//Loads the scene with the given ID. If the scene is empty it will declare it an unsuccesful
//load and attempt to quit early of the whole program
bool SceneManager::loadScene(std::string sceneID){
    currentScene = new Scene(sceneID);
    return  !currentScene->loadingError; //True if empty, so it's negated for startup
}
