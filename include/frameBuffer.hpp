#pragma once
#include "texture.hpp"
#include "cubeMap.hpp"
#include "glad/glad.h"
#include "displayManager.hpp"
//Main rendering buffers
struct FrameBuffer{
    void bind();
    void clear(GLbitfield clearTarget, glm::vec3 clearColor);
    void blitTo(const FrameBuffer &FBO, GLbitfield mask);

    void defaultInit();
    bool setupFrameBuffer();
    bool checkForCompleteness();

    int width, height;
    unsigned int frameBufferID;
    unsigned int texColorBuffer, depthBuffer;
};

struct FrameBufferMultiSampled : public FrameBuffer{
    bool setupFrameBuffer();
};

//Post Processing buffers
struct ResolveBuffer : public FrameBuffer{
    bool setupFrameBuffer();
    unsigned int blurHighEnd;
};

struct QuadHDRBuffer : public FrameBuffer{
    bool setupFrameBuffer();
};

struct CaptureBuffer : public FrameBuffer {
    void resizeFrameBuffer(int resolution);
    bool setupFrameBuffer(unsigned int w, unsigned int h);
};

//Shadow mapping frame buffers
struct DirShadowBuffer : public FrameBuffer{
    bool setupFrameBuffer(unsigned int w, unsigned int h);
};

struct PointShadowBuffer : public FrameBuffer{
    CubeMap drawingTexture;
    bool setupFrameBuffer(unsigned int w, unsigned int h);
};

void FrameBuffer::bind(){
    glViewport(0,0,width, height);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBufferID);
}

//TODO:: This currently clears whatever framebuffer is bound, not the framebuffer that calls
//this function
void FrameBuffer::clear(GLbitfield clearTarget, glm::vec3 clearColor){
    glClearColor(clearColor.r, clearColor.g, clearColor.b, 1.0f);
    glClear(clearTarget);
}

//Currently allows only for blit to one texture, not mrt blitting
void FrameBuffer::blitTo(const FrameBuffer &FBO, GLbitfield mask){

    glBindFramebuffer(GL_READ_FRAMEBUFFER, frameBufferID);

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, FBO.frameBufferID);

    if(( mask & GL_COLOR_BUFFER_BIT ) == GL_COLOR_BUFFER_BIT){
        glDrawBuffer(GL_COLOR_ATTACHMENT0);
    }

    glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, mask, GL_NEAREST );
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, frameBufferID);
}

//TODO:: include cases with width and height
void FrameBuffer::defaultInit(){
    width = DisplayManager::SCREEN_WIDTH;
    height = DisplayManager::SCREEN_HEIGHT;
    glGenFramebuffers(1, &frameBufferID);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBufferID);
}

//Check if frame buffer initialized correctly
bool FrameBuffer::checkForCompleteness(){
    if( glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE ){
        printf(" Failed to initialize the offscreen frame buffer!\n");
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        return false;
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    return true;
}

/*
Framebuffer Characteristics
1. 1 Color, 1 Depth buffer
2. Color Buffer: screen width/height, not multiSampled, HDR
3. Depth Buffer: screen width/height, not multiSampled, HDR
*/
bool FrameBuffer::setupFrameBuffer(){
    defaultInit();

    texColorBuffer = Texture::genTextureDirectlyOnGPU(width, height, 0, SING_2D_HDR_COL);
    depthBuffer    = Texture::genTextureDirectlyOnGPU(width, height, 0, SING_2D_HDR_DEP);

    return checkForCompleteness();
}

/*
Framebuffer Characteristics
1. 1 Color, 1 Depth buffer
2. Color Buffer: screen width/height, multisampled, HDR
3. Depth Buffer: screen width/height, multisampled, HDR
*/
bool FrameBufferMultiSampled::setupFrameBuffer(){
    defaultInit();

    texColorBuffer = Texture::genTextureDirectlyOnGPU(width, height, 0, MULT_2D_HDR_COL);
    depthBuffer    = Texture::genTextureDirectlyOnGPU(width, height, 0, MULT_2D_HDR_DEP);

    return checkForCompleteness();
}

/*
Framebuffer Characteristics
1. 2 Color, 1 Depth buffer
2. Color Buffer1: screen width/height, non multisampled, HDR
3. Color Buffer2: screen width/height, non multisampled, HDR, clamped to edges
3. Depth Buffer: screen width/height, non multisampled, HDR
*/
bool ResolveBuffer::setupFrameBuffer(){
    defaultInit();

    texColorBuffer = Texture::genTextureDirectlyOnGPU(width, height, 0, SING_2D_HDR_COL);
    blurHighEnd    = Texture::genTextureDirectlyOnGPU(width, height, 1, SING_2D_HDR_COL_CLAMP);
    depthBuffer    = Texture::genTextureDirectlyOnGPU(width, height, 0, SING_2D_HDR_DEP);

    return checkForCompleteness();
}

/*
Framebuffer Characteristics
1. 1 Color
2. Color Buffer: screen width/height, non multisampled, HDR , clamped to edges
*/
bool QuadHDRBuffer::setupFrameBuffer(){
    defaultInit();

    texColorBuffer = Texture::genTextureDirectlyOnGPU(width, height, 0, SING_2D_HDR_COL_CLAMP);

    return checkForCompleteness();
}

/*
Framebuffer Characteristics
1. 1 depth
2. depth render Buffer: user set width/height, non multisampled, HDR
*/
bool CaptureBuffer::setupFrameBuffer(unsigned int w, unsigned int h){
    width = w;
    height = h;
    glGenFramebuffers(1, &frameBufferID);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBufferID);

    glGenRenderbuffers(1, &depthBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    return checkForCompleteness();
}

void CaptureBuffer::resizeFrameBuffer(int resolution){
    glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, resolution, resolution);
}

/*
Framebuffer Characteristics
1. 1 depth
2. depth Buffer: texture, user set width/height, non multisampled, HDR, border color set to black
*/
bool DirShadowBuffer::setupFrameBuffer(unsigned int w, unsigned int h){
    width = w;
    height = h;
    glGenFramebuffers(1, &frameBufferID);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBufferID);

    depthBuffer = Texture::genTextureDirectlyOnGPU(width, height, 0, SING_2D_HDR_DEP_BORDER);

    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);

    return checkForCompleteness();
}

/*
Framebuffer Characteristics
1. 1 depth cubemap
2. depth Buffer: texture, user set width/height, non multisampled, HDR, border color set to black
*/
bool PointShadowBuffer::setupFrameBuffer(unsigned int w, unsigned int h){
    width = w;
    height = h;
    glGenFramebuffers(1, &frameBufferID);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBufferID);

    drawingTexture.generateCubeMap(width, height, SHADOW_MAP);
    depthBuffer = drawingTexture.textureID;
    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthBuffer, 0);

    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);

    return checkForCompleteness();
}

//-----------------------------------------------------------------------------------------------
//GeometryBuffer

struct GeometryBuffer{
    void bind();
    bool setupFrameBuffer();
    void blitTo(const FrameBuffer &FBO, GLbitfield mask);
    unsigned int frameBufferID, positionBuffer, normalsBuffer, albedoBuffer, emissiveBuffer ,lightBuffer, metalRoughBuffer, zBufferWStencil;
    int width, height;
};

bool GeometryBuffer::setupFrameBuffer(){
    width = DisplayManager::SCREEN_WIDTH;
    height = DisplayManager::SCREEN_HEIGHT;

    glGenFramebuffers(1, &frameBufferID);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBufferID);

    //Color+ Specular buffer
    glGenTextures(1, &albedoBuffer);
    glBindTexture(GL_TEXTURE_2D, albedoBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, albedoBuffer, 0);

    //Position buffer
    glGenTextures(1, &positionBuffer);
    glBindTexture(GL_TEXTURE_2D, positionBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, positionBuffer, 0);

    //Normal buffer
    glGenTextures(1, &normalsBuffer);
    glBindTexture(GL_TEXTURE_2D, normalsBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, normalsBuffer, 0);



    glGenTextures(1, &emissiveBuffer);
    glBindTexture(GL_TEXTURE_2D, emissiveBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, emissiveBuffer, 0);

    glGenTextures(1, &lightBuffer);
    glBindTexture(GL_TEXTURE_2D, lightBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, GL_TEXTURE_2D, lightBuffer, 0);

    glGenTextures(1, &metalRoughBuffer);
    glBindTexture(GL_TEXTURE_2D, metalRoughBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT5, GL_TEXTURE_2D, metalRoughBuffer, 0);

    //zBuffer？
    glGenRenderbuffers(1, &zBufferWStencil);
    glBindRenderbuffer(GL_RENDERBUFFER, zBufferWStencil);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);

    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    //Actually attaching to the frame buffer
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, zBufferWStencil);

    //Setting color attachments for rendering
    unsigned int attachments[6] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1,
                                   GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3,
                                   GL_COLOR_ATTACHMENT4, GL_COLOR_ATTACHMENT5};
    glDrawBuffers(6, attachments);

    if( glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE ){
        printf(" Failed to initialize the offscreen frame buffer!\n");
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        return false;
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    return true;
}

void GeometryBuffer::bind(){
    glViewport(0,0,width, height);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBufferID);
    glEnable(GL_DEPTH_TEST);
    // glClearColor(0.12f, 0.12f, 0.12f, 1.0f);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void GeometryBuffer::blitTo(const FrameBuffer &FBO, GLbitfield mask){
    //Specify the source framebuffer
    glBindFramebuffer(GL_READ_FRAMEBUFFER, frameBufferID);
    //destination framebuffer
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, FBO.frameBufferID);

    if(( mask & GL_COLOR_BUFFER_BIT ) == GL_COLOR_BUFFER_BIT){
        glDrawBuffer(GL_COLOR_ATTACHMENT0);
    }
    //Copy source buffer to destination buffer
    glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, mask, GL_NEAREST );
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, frameBufferID);
}