#include "geometry.hpp"
#include "model.hpp"

struct BVHBuildNode {
    AABox aabox;
    BVHBuildNode *left;
    BVHBuildNode *right;
    Model* model;
    float area;

public:
    int splitAxis=0, firstPrimOffset=0, nPrimitives=0;
    // BVHBuildNode Public Methods
    BVHBuildNode(){
        aabox = AABox();
        left = nullptr;right = nullptr;
        model = nullptr;
    }
};

//class BVHAccel {
//
//public:
//    // BVHAccel Public Types
//    enum class SplitMethod { NAIVE, SAH };
//
//    // BVHAccel Public Methods
//    BVHAccel(std::vector<Model*> p, int maxPrimsInNode = 1, SplitMethod splitMethod = SplitMethod::NAIVE);
//    AABox WorldBound() const;
//    ~BVHAccel();
//
//    BVHBuildNode* root;
//
//    // BVHAccel Private Methods
//    BVHBuildNode* recursiveBuild(std::vector<Model*>objects);
//
//    // BVHAccel Private Data
//    const int maxPrimsInNode;
//    const SplitMethod splitMethod;
//    std::vector<Model*> primitives;
//
//};
//
//BVHAccel::BVHAccel(std::vector<Model*> p, int maxPrimsInNode,
//                   SplitMethod splitMethod)
//        : maxPrimsInNode(std::min(255, maxPrimsInNode)), splitMethod(splitMethod),
//          primitives(std::move(p))
//{
//    time_t start, stop;
//    time(&start);
//    if (primitives.empty())
//        return;
//
//    root = recursiveBuild(primitives);
//
//    time(&stop);
//    double diff = difftime(stop, start);
//    int hrs = (int)diff / 3600;
//    int mins = ((int)diff / 60) - (hrs * 60);
//    int secs = (int)diff - (hrs * 3600) - (mins * 60);
//
//    printf(
//            "\rBVH Generation complete: \nTime Taken: %i hrs, %i mins, %i secs\n\n",
//            hrs, mins, secs);
//}
