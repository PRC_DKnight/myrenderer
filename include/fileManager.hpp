#pragma once
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <iostream>
#include <fstream>

namespace FLOAD{
    bool checkFileValidity(const std::string &filePath);
    std::string getFileExtension(const std::string &filePath);
}

bool FLOAD::checkFileValidity(const std::string &filePath){
    // The _stat structure is a structure of file (folder) information
    struct stat info;
    //file is blocking access or read
    if( stat( filePath.c_str(), &info ) != 0 ){
        printf( "Cannot access %s\n", filePath.c_str() );
        return false;
    }
        //file is accessible S_IFMT是文件类型的位掩码
    else if( info.st_mode & S_IFMT ){
        printf( "%s is a valid file\n", filePath.c_str() );
        return true;
    }
        //File does not exist
    else{
        printf("Error! File: %s does not exist.\n", filePath.c_str());
        return false;
    }
}

// 获取最后一个点的索引位置并返回点后的字符。如果找不到点，则返回空标记
std::string FLOAD::getFileExtension(const std::string &filePath){
    size_t indexLocation = filePath.rfind('.', filePath.length());
    if( indexLocation != std::string::npos){
        return  filePath.substr(indexLocation + 1, filePath.length() - indexLocation);
    }
    return "";
}