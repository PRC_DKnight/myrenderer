#include "glm/glm.hpp"
#include "geometry.hpp"
class Object{
public:
    Object() {}
    virtual ~Object() {}
    virtual AABox getAABox()=0;
    virtual float getArea()=0;
};