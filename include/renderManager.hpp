#pragma once
#include "displayManager.hpp"
#include "sceneManager.hpp"
#include "frameBuffer.hpp"
#include "skybox.hpp"
#include "shader.hpp"
#include "camera.hpp"
#include "model.hpp"
#include "mesh.hpp"
#include <vector>
#include "imgui/imgui.h"
#include "debugUtils.hpp"
#include "gpuData.hpp"
#include "cmath"
#include "memory"

enum RenderMode{
    ForWardPlus,
    Deferred
};

class RenderManager{
public:
    //Dummy constructors / Destructors
    RenderManager();
    ~RenderManager();

    //Gets scene and display info. Will be used to build render Queue
    bool startUp(DisplayManager &displayManager, SceneManager &sceneManager);
    void shutDown();

    //The core rendering loop of the engine. The steps that it follow are indicated on the cpp file
    //but it's just a very vanilla Forward+ clustered renderer with barely any bells and whistled or
    //optimizations. In the future, (the magical land where all projects are complete) I plan on
    //heavily optimizing this part of the program along the lines of the 2014 talk, "beyond porting"
    //But in the mean-time it uses pretty basic an naive openGL.
    void render(const unsigned int start);
private:
    //Internal initialization functions
    bool initFBOs();
    bool initSSBOs();
    bool loadShaders();
    //TODO:: rewrite shadow mapping to be dynamic and fast and make use of cluster shading
    //and some new low driver overhead stuff I've been reading about
    bool preProcess();
    void drawUI();
    void drawDeferred();
    //Functions used to break up the main render function into more manageable parts.
    void postProcess(const unsigned int start);
    // Forward+ clustered rendering or Deferred rendering
    //TODO:Consider separating different kinds of renderers？
    RenderMode renderMode=Deferred;
    //Todo:: shaders should belong to a material not the rendermanager
    Shader depthPrePassShader, PBRClusteredShader, skyboxShader,
            highPassFilterShader, gaussianBlurShader, screenSpaceShader,
            dirShadowShader, pointShadowShader, fillCubeMapShader,
            convolveCubeMap, preFilterSpecShader, integrateBRDFShader;

    Shader gBufferShader,PBRDeferredShader;
    QuadHDRBuffer deferredFBO;
    GeometryBuffer gBuffer;
    //TODO::Compute shaders don't have a strong a case as regular shaders to be made a part of
    //other classes, since they feel more like static functions of the renderer than methods that
    //are a part of certain objects.
    ComputeShader buildAABBGridCompShader, cullLightsCompShader;

    //Pointers to the scene and its contents which contains all the geometry data
    //that we will be rendering rendering
    Camera*   sceneCamera;
    Scene*  currentScene;
    DisplayManager* screen;
    SceneManager*   sceneLocator;

    //The canvas is an abstraction for screen space rendering. It helped me build a mental model
    //of drawing at the time but I think it is now unecessary sinceI feel much more comfortable with
    //compute shaders and the inner workings of the GPU.
    Quad canvas;

    //The variables that determine the size of the cluster grid. They're hand picked for now, but
    //there is some space for optimization and tinkering as seen on the Olsson paper and the ID tech6
    //presentation.
    const unsigned int gridSizeX = 16;
    const unsigned int gridSizeY =  9;
    const unsigned int gridSizeZ = 24;
    const unsigned int numClusters = gridSizeX * gridSizeY * gridSizeZ;
    unsigned int sizeX, sizeY;

    unsigned int numLights;
    const unsigned int maxLights = 1000; // pretty overkill for sponza, but ok for testing
    const unsigned int maxLightsPerTile = 50;

    //Shader buffer objects, currently completely managed by the rendermanager class for creation
    //using and uploading to the gpu, but they should be moved somwehre else to avoid bloat
    unsigned int AABBvolumeGridSSBO, screenToViewSSBO;
    unsigned int lightSSBO, lightIndexListSSBO, lightGridSSBO, lightIndexGlobalCountSSBO;

    //Render pipeline FrameBuffer objects.
    ResolveBuffer simpleFBO;
    CaptureBuffer captureFBO;
    QuadHDRBuffer pingPongFBO;
    DirShadowBuffer  dirShadowFBO;
    FrameBufferMultiSampled multiSampledFBO;
    PointShadowBuffer *pointLightShadowFBOs;
};

RenderManager::RenderManager(){}
RenderManager::~RenderManager(){}

//Sets the internal pointers to the screen and the current scene and inits the software
//renderer instance.
bool RenderManager::startUp(DisplayManager &displayManager, SceneManager &sceneManager ){
    printf("\nInitializing Renderer.\n");
    //Getting pointers to the data we'll render
    screen =  &displayManager;
    sceneLocator = &sceneManager;
    currentScene = sceneLocator->getCurrentScene();
    sceneCamera = currentScene->getCurrentCamera();

    printf("Loading FBO's...\n");
    if( !initFBOs() ){
        printf("FBO's failed to be initialized correctly.\n");
        return false;
    }

    printf("Loading Shaders...\n");
    if (!loadShaders()){
        printf("Shaders failed to be initialized correctly.\n");
        return false;
    }

    printf("Loading SSBO's...\n");
    if (!initSSBOs()){
        printf("SSBO's failed to be initialized correctly.\n");
        return false;
    }

    printf("Preprocessing...\n");
    if (!preProcess()){
        printf("SSBO's failed to be initialized correctly.\n");
        return false;
    }

    printf("Renderer Initialization complete.\n");
    return true;
}





//TODO:: some of the buffer generation and binding should be abstracted into a function
bool RenderManager::initSSBOs(){
    //Setting up tile size on both X and Y
    sizeX =  (unsigned int)ceilf(DisplayManager::SCREEN_WIDTH / (float)gridSizeX);

    float zFar    =  sceneCamera->cameraFrustum.farPlane;
    float zNear   =  sceneCamera->cameraFrustum.nearPlane;

    //Buffer containing all the clusters
    {
        glGenBuffers(1, &AABBvolumeGridSSBO);
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, AABBvolumeGridSSBO);

        //We generate the buffer but don't populate it yet.
        glBufferData(GL_SHADER_STORAGE_BUFFER, numClusters * sizeof(struct VolumeTileAABB), NULL, GL_STATIC_COPY);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, AABBvolumeGridSSBO);
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
    }

    //Setting up screen2View ssbo
    {
        glGenBuffers(1, &screenToViewSSBO);
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, screenToViewSSBO);

        //Setting up contents of buffer
        screen2View.inverseProjectionMat = glm::inverse(sceneCamera->projectionMatrix);
        screen2View.tileSizes[0] = gridSizeX;
        screen2View.tileSizes[1] = gridSizeY;
        screen2View.tileSizes[2] = gridSizeZ;
        screen2View.tileSizes[3] = sizeX;
        screen2View.screenWidth  = DisplayManager::SCREEN_WIDTH;
        screen2View.screenHeight = DisplayManager::SCREEN_HEIGHT;
        //Basically reduced a log function into a simple multiplication an addition by pre-calculating these
        screen2View.sliceScalingFactor = (float)gridSizeZ / std::log2f(zFar / zNear) ;
        screen2View.sliceBiasFactor    = -((float)gridSizeZ * std::log2f(zNear) / std::log2f(zFar / zNear)) ;

        //Generating and copying data to memory in GPU
        glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(struct ScreenToView), &screen2View, GL_STATIC_COPY);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, screenToViewSSBO);
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
    }

    //Setting up lights buffer that contains all the lights in the scene
    {
        glGenBuffers(1, &lightSSBO);
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, lightSSBO);
        glBufferData(GL_SHADER_STORAGE_BUFFER, maxLights * sizeof(struct GPULight), NULL, GL_DYNAMIC_DRAW);

        GLint bufMask = GL_READ_WRITE;

        struct GPULight *lights = (struct GPULight *)glMapBuffer(GL_SHADER_STORAGE_BUFFER, bufMask);
        PointLight *light;
        for(unsigned int i = 0; i < numLights; ++i ){
            //Fetching the light from the current scene
            light = currentScene->getPointLight(i);
            lights[i].position  = glm::vec4(light->position, 1.0f);
            lights[i].color     = glm::vec4(light->color, 1.0f);
            lights[i].enabled   = 1;
            lights[i].intensity = 1.0f;
            lights[i].range     = 65.0f;
        }
        glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, lightSSBO);
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
    }

    //A list of indices to the lights that are active and intersect with a cluster
    {
        unsigned int totalNumLights =  numClusters * maxLightsPerTile; //50 lights per tile max
        glGenBuffers(1, &lightIndexListSSBO);
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, lightIndexListSSBO);

        //We generate the buffer but don't populate it yet.
        glBufferData(GL_SHADER_STORAGE_BUFFER,  totalNumLights * sizeof(unsigned int), NULL, GL_STATIC_COPY);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, lightIndexListSSBO);
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
    }

    //Every tile takes two unsigned ints one to represent the number of lights in that grid
    //Another to represent the offset to the light index list from where to begin reading light indexes from
    //This implementation is straight up from Olsson paper
    {
        glGenBuffers(1, &lightGridSSBO);
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, lightGridSSBO);

        glBufferData(GL_SHADER_STORAGE_BUFFER, numClusters * 2 * sizeof(unsigned int), NULL, GL_STATIC_COPY);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 5, lightGridSSBO);
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
    }

    //Setting up simplest ssbo in the world
    {
        glGenBuffers(1, &lightIndexGlobalCountSSBO);
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, lightIndexGlobalCountSSBO);

        //Every tile takes two unsigned ints one to represent the number of lights in that grid
        //Another to represent the offset
        glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(unsigned int), NULL, GL_STATIC_COPY);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 6, lightIndexGlobalCountSSBO);
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
    }

    return true;
}

bool RenderManager::loadShaders(){
    bool stillValid = true;
    //Pre-processing
    stillValid &= buildAABBGridCompShader.setup("clusterShader.comp");
    stillValid &= cullLightsCompShader.setup("clusterCullLightShader.comp");
    stillValid &= fillCubeMapShader.setup("cubeMapShader.vert", "buildCubeMapShader.frag");
    stillValid &= convolveCubeMap.setup("cubeMapShader.vert", "convolveCubemapShader.frag");
    stillValid &= preFilterSpecShader.setup("cubeMapShader.vert", "preFilteringShader.frag");
    stillValid &= integrateBRDFShader.setup("screenShader.vert", "brdfIntegralShader.frag");

    if(!stillValid){
        printf("Error loading pre-processing Shaders!\n");
        return false;
    }
    //Rendering
    stillValid &= depthPrePassShader.setup("depthPassShader.vert", "depthPassShader.frag");
    stillValid &= skyboxShader.setup("skyboxShader.vert", "skyboxShader.frag");
    stillValid &= screenSpaceShader.setup("screenShader.vert", "screenShader.frag");
    stillValid &= gBufferShader.setup("gBufferShader.vert", "gBufferShader.frag");
    if(renderMode==Deferred){
        stillValid &= PBRDeferredShader.setup("PBRDeferredShader.vert", "PBRDeferredShader.frag");
    }else if(renderMode==ForWardPlus){
        stillValid &= PBRClusteredShader.setup("PBRClusteredShader.vert", "PBRClusteredShader.frag");
    }
    if(!stillValid){
        printf("Error loading rendering Shaders!\n");
        return false;
    }

    //Shadow mapping
    stillValid &= dirShadowShader.setup("shadowShader.vert", "shadowShader.frag");
    stillValid &= pointShadowShader.setup("pointShadowShader.vert", "pointShadowShader.frag", "pointShadowShader.geom");

    if(!stillValid){
        printf("Error loading shadow mapping Shaders!\n");
        return false;
    }
    //Post-processing
    stillValid &= highPassFilterShader.setup("splitHighShader.vert", "splitHighShader.frag");
    stillValid &= gaussianBlurShader.setup("blurShader.vert", "blurShader.frag");

    if(!stillValid){
        printf("Error loading post-processing Shaders!\n");
        return false;
    }

    return stillValid;
}

void RenderManager::shutDown(){
    screen = nullptr;
    sceneCamera  = nullptr;
    sceneLocator = nullptr;
}

bool RenderManager::initFBOs(){
    //Init variables
    unsigned int shadowMapResolution = currentScene->getShadowRes();
    int skyboxRes = currentScene->mainSkyBox.resolution;
    numLights = currentScene->pointLightCount;
    bool stillValid = true;

    //Shadow Framebuffers
    pointLightShadowFBOs = new PointShadowBuffer[numLights];

    //Directional light
    stillValid  &= dirShadowFBO.setupFrameBuffer(shadowMapResolution, shadowMapResolution);

    //Point light
    for(unsigned int i = 0; i < numLights; ++i ){
        stillValid &= pointLightShadowFBOs[i].setupFrameBuffer(shadowMapResolution, shadowMapResolution);
    }

    if(!stillValid){
        printf("Error initializing shadow map FBO's!\n");
        return false;
    }

    //Rendering buffers
    stillValid &= multiSampledFBO.setupFrameBuffer();
    stillValid &= captureFBO.setupFrameBuffer(skyboxRes, skyboxRes);
    if(renderMode==Deferred){
        stillValid &= gBuffer.setupFrameBuffer();
        stillValid &= deferredFBO.setupFrameBuffer();
    }
    //Deferred G-buffer

    if(!stillValid){
        printf("Error initializing rendering FBO's!\n");
        return false;
    }

    //Post processing buffers
    stillValid &= pingPongFBO.setupFrameBuffer();
    stillValid &= simpleFBO.setupFrameBuffer();

    if(!stillValid){
        printf("Error initializing postPRocessing FBO's!\n");
        return false;
    }

    return stillValid;
}

bool RenderManager::preProcess(){
    //Initializing the surface that we use to draw screen-space effects
    canvas.setup();

    //Building the grid of AABB enclosing the view frustum clusters
    buildAABBGridCompShader.use();
    buildAABBGridCompShader.setFloat("zNear", sceneCamera->cameraFrustum.nearPlane);
    buildAABBGridCompShader.setFloat("zFar", sceneCamera->cameraFrustum.farPlane);
    buildAABBGridCompShader.dispatch(gridSizeX, gridSizeY, gridSizeZ);

    //Environment Mapping
    //Passing equirectangular map to cubemap
    captureFBO.bind();
    currentScene->mainSkyBox.fillCubeMapWithTexture(fillCubeMapShader);

    //Cubemap convolution TODO:: This could probably be moved to a function of the scene or environment maps
    //themselves as a class / static function
    int res = currentScene->irradianceMap.width;
    captureFBO.resizeFrameBuffer(res);
    unsigned int environmentID = currentScene->mainSkyBox.skyBoxCubeMap.textureID;
    currentScene->irradianceMap.convolveCubeMap(environmentID, convolveCubeMap);

    //Cubemap prefiltering TODO:: Same as above
    unsigned int captureRBO = captureFBO.depthBuffer;
    currentScene->specFilteredMap.preFilterCubeMap(environmentID, captureRBO, preFilterSpecShader);

    //BRDF lookup texture
    integrateBRDFShader.use();
    res = currentScene->brdfLUTTexture.height;
    captureFBO.resizeFrameBuffer(res);
    unsigned int id = currentScene->brdfLUTTexture.textureID;
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, id, 0);
    glViewport(0, 0, res, res);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    canvas.draw();

    //Making sure that the viewport is the correct size after rendering
    glViewport(0, 0, DisplayManager::SCREEN_WIDTH, DisplayManager::SCREEN_HEIGHT);

    glEnable(GL_DEPTH_TEST);
    glDepthMask(true);

    //Populating depth cube maps for the point light shadows
    //Point lights do not currently move in the scene, so they are not included in the calculations per frame
    for (unsigned int i = 0; i < currentScene->pointLightCount; ++i){
        pointLightShadowFBOs[i].bind();
        pointLightShadowFBOs[i].clear(GL_DEPTH_BUFFER_BIT, glm::vec3(1.0f));
        currentScene->drawPointLightShadow(pointShadowShader,i, pointLightShadowFBOs[i].depthBuffer);
    }

    //As we add more error checking this will change from a dummy variable to an actual thing
    return true;
}
void RenderManager::drawUI(){
    //Initiating rendering gui
    ImGui::Begin("Rendering Controls");
    ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

    if(ImGui::CollapsingHeader("Controls")){
        ImGui::Text("Strafe: w a s d");
        ImGui::Text("Rotate Camera: hold left click + mouse");
        ImGui::Text("Up&Down: q e");
        ImGui::Text("Reset Camera: r");
        ImGui::Text("Exit: ESC");
        ImGui::InputFloat3("Camera Pos", (float*)&sceneCamera->position); //Camera controls
        ImGui::SliderFloat("Movement speed", &sceneCamera->camSpeed, 0.005f, 1.0f);
    }

    if(ImGui::CollapsingHeader("Models")){
        auto* visibleModels=currentScene->getVisibleModels();
        for(int i=0;i<visibleModels->size();i++){
            ImGui::Text(("Model id:"+std::to_string(i)).c_str());
            ImGui::SliderFloat3("Position", (float*)&(*visibleModels)[i]->curTransform.translation,-100.0f, 100.0f);
            ImGui::SliderFloat("Rotation X", (float*)&(*visibleModels)[i]->curTransform.angleX,-180, 180);
            ImGui::SliderFloat("Rotation Y", (float*)&(*visibleModels)[i]->curTransform.angleY,-180, 180);
            ImGui::SliderFloat("Rotation Z", (float*)&(*visibleModels)[i]->curTransform.angleZ,-180, 180);
            ImGui::InputFloat3("Scale", (float*)&(*visibleModels)[i]->curTransform.scaling);
        }
    }
    if(ImGui::CollapsingHeader("Directional Light Settings")){
        ImGui::TextColored(ImVec4(1,1,1,1), "Directional light Settings");
        ImGui::ColorEdit3("Color", (float *)&currentScene->dirLight.color);
        ImGui::SliderFloat("Strength", &currentScene->dirLight.strength, 0.1f, 200.0f);
        ImGui::SliderFloat("BoxSize", &currentScene->dirLight.orthoBoxSize, 0.1f, 500.0f);
        ImGui::SliderFloat3("Direction", (float*)&currentScene->dirLight.direction, -5.0f, 5.0f);
    }
    if(ImGui::CollapsingHeader("Post-processing")){
        ImGui::SliderInt("Blur", &sceneCamera->blurAmount, 0, 10);
        ImGui::SliderFloat("Exposure", &sceneCamera->exposure, 0.1f, 5.0f);
    }
    if(renderMode==ForWardPlus){
        if(ImGui::CollapsingHeader("Cluster Debugging Light Settings")){
            ImGui::Checkbox("Display depth Slices", &currentScene->slices);
        }
    }
    //Rendering gui scope ends here cannot be done later because the whole frame
    //is reset in the display buffer swap
    ImGui::End();
}
/* This time using volume tiled forward
Algorithm steps:
//Initialization or view frustrum change
0. Determine AABB's for each volume
//Update Every frame
1. Depth-pre pass :: DONE
2. Mark Active tiles :: POSTPONED AS OPTIMIZATION
3. Build Tile list ::  POSTPONED AS OPTIMIZATION
4. Assign lights to tiles :: DONE (BUT SHOULD BE OPTIMIZED)
5. Shading by reading from the active tiles list :: DONE
6. Post processing and screen space effects :: DONE
*/

void RenderManager::render(const unsigned int start){
    drawUI();
    //Making sure depth testing is enabled
    glEnable(GL_DEPTH_TEST);
    glDepthMask(true);
    // Directional shadows
    dirShadowFBO.bind();
    dirShadowFBO.clear(GL_DEPTH_BUFFER_BIT, glm::vec3(1.0f));
    currentScene->drawDirLightShadows(dirShadowShader, dirShadowFBO.depthBuffer);
    if(renderMode==ForWardPlus){
        //1.1- Multisampled Depth pre-pass
        multiSampledFBO.bind();
        multiSampledFBO.clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT, glm::vec3(0.0f));
        currentScene->drawDepthPass(depthPrePassShader);

        //4-Light assignment
        cullLightsCompShader.use();
        cullLightsCompShader.setMat4("viewMatrix", sceneCamera->viewMatrix);
        cullLightsCompShader.dispatch(1,1,6);

        //5 - Actual shading;
        //5.1 - Forward render the scene in the multisampled FBO using the z buffer to discard early
        glDepthFunc(GL_LEQUAL);
        glDepthMask(false);
        currentScene->drawFullScene(PBRClusteredShader, skyboxShader);

        //5.2 - resolve the from multisampled to normal resolution for postProcessing
        multiSampledFBO.blitTo(simpleFBO, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }else{
        //Deffered rendering
        //Filling the geometry buffer
        gBuffer.bind();
        currentScene->drawGeometry(gBufferShader, skyboxShader);

        //shading pass
        deferredFBO.bind();
        currentScene->setupDeferredShader(PBRDeferredShader, skyboxShader);
        drawDeferred();
        deferredFBO.blitTo(simpleFBO, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }
    //postprocessing, includes bloom, exposure mapping
    postProcess(start);

    //Drawing to the screen by swapping the window's surface with the
    //final buffer containing all rendering information
    screen->swapDisplayBuffer();
}


void RenderManager::postProcess(const unsigned int start){
    //TODO:: should be a compute shader
    pingPongFBO.bind();
    pingPongFBO.clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT, glm::vec3(0.0f));
    if( sceneCamera->blurAmount > 0){
        //Filtering pixel rgb values > 1.0
        highPassFilterShader.use();
        canvas.draw(simpleFBO.texColorBuffer);
    }

    //Applying Gaussian blur in ping pong fashion
    //TODO:: ALso make it a compute shader
    gaussianBlurShader.use();
    for (int i = 0; i < sceneCamera->blurAmount; ++i){
        //Horizontal pass
        glBindFramebuffer(GL_FRAMEBUFFER, simpleFBO.frameBufferID);
        glDrawBuffer(GL_COLOR_ATTACHMENT1);
        gaussianBlurShader.setBool("horizontal", true);
        canvas.draw(pingPongFBO.texColorBuffer);

        //Vertical pass
        glBindFramebuffer(GL_FRAMEBUFFER, pingPongFBO.frameBufferID);
        gaussianBlurShader.setBool("horizontal", false);
        canvas.draw(simpleFBO.blurHighEnd);
    }
    //Setting back to default framebuffer (screen) and clearing
    //No need for depth testing cause we're drawing to a flat quad
    screen->bind();

    //Shader setup for postprocessing
    screenSpaceShader.use();

    screenSpaceShader.setInt("offset", start);
    screenSpaceShader.setFloat("exposure", sceneCamera->exposure);
    screenSpaceShader.setInt("screenTexture", 0);
    screenSpaceShader.setInt("bloomBlur", 1);
    screenSpaceShader.setInt("computeTexture", 2);

    //Merging the blurred high pass image with the low pass values
    //Also tonemapping and doing other post processing
    canvas.draw(simpleFBO.texColorBuffer, pingPongFBO.texColorBuffer);
}

void RenderManager::drawDeferred(){
    glBindVertexArray(canvas.VAO);
    // glDisable(GL_DEPTH_TEST);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D,gBuffer.albedoBuffer);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D,gBuffer.positionBuffer);

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D,gBuffer.normalsBuffer);

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D,gBuffer.emissiveBuffer);

    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_2D,gBuffer.lightBuffer);

    glActiveTexture(GL_TEXTURE5);
    glBindTexture(GL_TEXTURE_2D,gBuffer.metalRoughBuffer);

    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
}