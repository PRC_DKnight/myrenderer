#pragma once
#include "shader.hpp"
#include "texture.hpp"
#include "mesh.hpp"
#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include <vector>
#include <unordered_map>
#include <string>
#include "object.hpp"

#include "geometry.hpp"
#include "glm/glm.hpp"
#include "fileManager.hpp"
#include <string>
struct TransformParameters{
    TransformParameters(): scaling(glm::vec3(1.0f)) {};
    glm::vec3 translation;
    float angleX;
    float angleY;
    float angleZ;
    glm::vec3 scaling;
};
static int GlobalModelId=0;

struct Model:Object {
    Model(const std::string meshPath, const TransformParameters initParameters, bool IBL) : IBL(IBL),curTransform(initParameters){
        loadModel(meshPath);
        modelId=++GlobalModelId;
    }
    TransformParameters curTransform;
    //Base interface
    void loadModel(std::string path);
    void update(const unsigned int deltaT);
    void draw(const Shader &shader, const bool textured);

    //Model processing/loading functions
    void processNode(aiNode *node, const aiScene *scene);
    Mesh processMesh(aiMesh *mesh, const aiScene *scene);
    std::vector<unsigned int> processTextures(const aiMaterial *material);
    AABox getAABox(){
        return aaBox;
    }
    float getArea(){
        return area;
    }
    float area;
    int modelId=0;
    //Object to world space matrix
    bool IBL;
    glm::mat4 modelMatrix;
    std::vector<Mesh> meshes; //Does it need to be a vector after initialization?

    //To avoid textures being loaded from disk more than once they are indexed into a dictionary
    std::unordered_map<std::string, Texture> textureAtlas;
    std::string directory, fileExtension;
    AABox aaBox;
};

//We use assimp to load all our mesh files this
void Model::loadModel(std::string path){
    Assimp::Importer importer;
    const aiScene *scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_OptimizeMeshes |aiProcess_CalcTangentSpace | aiProcess_FlipUVs);

    //useful for texture indexing later
    fileExtension = FLOAD::getFileExtension(path);
    directory = path.substr(0, path.find_last_of('/'));
    directory += "/";

    //begin recursive processing of loaded model
    processNode(scene->mRootNode, scene);

}

//The model currently is just a vessel for the meshes of the scene,
//In a future revision this will probably change
void Model::draw(const Shader &shader, const  bool textured){
    shader.setBool("IBL", IBL);
    for(int i = 0; i < meshes.size(); ++i){
        meshes[i].draw(shader, textured,modelId);
    }
}

//Stub, to fill in later
void Model::update(const unsigned int deltaT){
    modelMatrix = glm::mat4(1.0);
    modelMatrix = glm::translate(modelMatrix, curTransform.translation);

    modelMatrix = glm::rotate(modelMatrix, glm::radians(curTransform.angleX), glm::vec3(1.0,0.0,0.0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(curTransform.angleY), glm::vec3(0.0,1.0,0.0));
    modelMatrix = glm::rotate(modelMatrix, glm::radians(curTransform.angleZ), glm::vec3(0.0,0.0,1.0));

    modelMatrix = glm::scale(modelMatrix, curTransform.scaling);
}

//Basic ASSIMP scene tree traversal, taken from the docs
void Model::processNode(aiNode *node, const aiScene *scene){
    //Process all the node meshes
    for(unsigned int i = 0; i < node->mNumMeshes; i++){
        aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
        meshes.push_back(processMesh(mesh,scene));
    }

    //process all the node children recursively
    for(unsigned int i = 0; i < node->mNumChildren; i++){
        processNode(node->mChildren[i], scene);
    }
}

Mesh Model::processMesh(aiMesh *mesh, const aiScene *scene){
    std::vector<Vertex> vertices;
    std::vector<unsigned int > indices;
    std::vector<unsigned int > textures;

    //Process vertices
    for(unsigned int i = 0; i < mesh->mNumVertices; ++i){
        //Process vertex positions, normals, tangents, bitangents, and texture coordinates
        Vertex vertex;
        glm::vec3 vector;

        //Process position
        vector.x = mesh->mVertices[i].x;
        vector.y = mesh->mVertices[i].y;
        vector.z = mesh->mVertices[i].z;
        vertex.position = vector;

        //Process tangent
        vector.x = mesh->mTangents[i].x;
        vector.y = mesh->mTangents[i].y;
        vector.z = mesh->mTangents[i].z;
        vertex.tangent = vector;

        //Process biTangent
        vector.x = mesh->mBitangents[i].x;
        vector.y = mesh->mBitangents[i].y;
        vector.z = mesh->mBitangents[i].z;
        vertex.biTangent = vector;

        //Process normals
        vector.x = mesh->mNormals[i].x;
        vector.y = mesh->mNormals[i].y;
        vector.z = mesh->mNormals[i].z;
        vertex.normal = vector;

        //Process texture coords
        if (mesh->HasTextureCoords(0)){
            glm::vec2 vec;
            vec.x = mesh->mTextureCoords[0][i].x;
            vec.y = mesh->mTextureCoords[0][i].y;
            vertex.texCoords = vec;
        }
        else{
            vertex.texCoords = glm::vec2(0.0f, 0.0f);
        }

        vertices.push_back(vertex);
    }

    //Process indices
    for(unsigned int i = 0; i < mesh->mNumFaces; ++i){
        aiFace face = mesh->mFaces[i];
        for(unsigned int j = 0; j < face.mNumIndices; ++j){
            indices.push_back(face.mIndices[j]);
        }
    }

    //Process material and texture info
    aiMaterial *material = scene->mMaterials[mesh->mMaterialIndex];
    textures = processTextures(material);
    auto resMesh = Mesh(vertices, indices, textures);
    aaBox=AABoxUnion(aaBox,resMesh.aaBox);
    return resMesh;
}

/*
FIXES::
1. Have more than one texture per type
2. Make this it's own material class that takes care of it properly
*/
std::vector<unsigned int> Model::processTextures(const aiMaterial *material){
    std::vector<unsigned int> textures;

    //Finding current texture directory
    aiString texturePath;
    aiTextureType type;
    std::string fullTexturePath;

    //Checking all texture stacks for each texture type
    //Checkout assimp docs on texture types
    for(int tex = aiTextureType_NONE ; tex <= aiTextureType_UNKNOWN; tex++){
        type = static_cast<aiTextureType>(tex); //making the int value into the enum value
        fullTexturePath = directory;

        //If there are any textures of the given type in the material
        if( material->GetTextureCount(type) > 0 ){
            //We only care about the first texture assigned we don't expect multiple to be assigned
            material->GetTexture(type, 0, &texturePath);
            fullTexturePath = fullTexturePath.append(texturePath.C_Str());

            //If this texture has not been added to the atlas yet we load it
            if (textureAtlas.count(fullTexturePath) == 0){
                Texture texture;
                bool srgb = false;
                texture.loadTexture(fullTexturePath, srgb);
                textureAtlas.insert({fullTexturePath, texture});
            }

            //We add it to the texture index array of loaded texture for a given mesh
            bool hasLoaded=false;
            for(auto id:textures){
                if(id==textureAtlas.at(fullTexturePath).textureID){
                    hasLoaded=true;
                }
            }
            if(!hasLoaded)textures.push_back(textureAtlas.at(fullTexturePath).textureID);
        }
        else{
            //For now we always assume that these textures will exist in the current
            //material. If they do not, we assign 0 to their value.
            //This will be fixed when the new material model is implemented.
            switch(type){
                case aiTextureType_LIGHTMAP:
                case aiTextureType_EMISSIVE:
                case aiTextureType_NORMALS:
                case aiTextureType_UNKNOWN:
                    textures.push_back(0);
                    break;
            }
        }
    }
    return textures;
}