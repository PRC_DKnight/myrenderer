#pragma once
#include "geometry.hpp"
#include "displayManager.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include <set>

struct Camera{
    Camera(glm::vec3 tar, glm::vec3 pos, float fov,
           float speed, float sens, float nearP, float farP)
    {
        //Position and orientation of the camera, both in cartesian and spherical
        position = pos;
        target   = tar;
        front = glm::normalize(target - position);
        side  = glm::normalize(glm::cross(front, up));
        pitch = getPitch(front);
        yaw   = getYaw(front, pitch);

        //Saving reset position values
        originalPosition = pos;
        originalTarget   = tar;
        originalFront    = front;
        originalSide     = side;
        originalPitch    = pitch;
        originalYaw      = yaw;

        //Shaping the frustum to the scene's imported values
        cameraFrustum.fov = fov;
        cameraFrustum.AR  = DisplayManager::SCREEN_ASPECT_RATIO;
        cameraFrustum.farPlane  = farP;
        cameraFrustum.nearPlane = nearP;

        //Setting default values of other miscellaneous camera parameters
        camSpeed   = speed;
        mouseSens  = sens;
        blurAmount = 0;
        exposure   = 1.0f;

        //Setting up perspective and view matrix for rendering
        viewMatrix = glm::lookAt(position, target, up);
        projectionMatrix = glm::perspective(glm::radians(cameraFrustum.fov),
                                            cameraFrustum.AR, cameraFrustum.nearPlane,
                                            cameraFrustum.farPlane);
        cameraFrustum.updatePlanes(viewMatrix, position);
    }

    //Updates the camera matrices with the user input
    void update(unsigned int deltaT);
    void resetCamera();

    //Cartesian to spherical coordinates
    float getPitch(glm::vec3 front);
    float getYaw(glm::vec3 front, float pitch);
    void updateOrientation();

    //View frustrum culling
    bool checkVisibility(AABox *bounds);

    //Containers for the camera transform matrices and frustum geometry
    glm::mat4 viewMatrix, projectionMatrix;
    Frustum cameraFrustum;

    //Keeps track of the current relevant keys that are pressed to avoid issues with
    //the frequency of polling of the keyboard vs the frequency of updates
    std::set<char> activeMoveStates;

    //Original values used to initialize the camera
    //We keep them in memory in case user wants to reset position
    glm::vec3 originalPosition, originalTarget, originalFront, originalSide;
    float originalPitch, originalYaw;

    //Camera basis vectors for view matrix construction
    glm::vec3 position, side, front, target, up{0,1,0};
    float pitch, yaw;

    //Physical/Optical properties
    float camSpeed, mouseSens, exposure;
    int blurAmount;
};

//Updates the cameras orientation and position based on the input from the user
///Also updates view matrix and projection matrix for rendering
void Camera::update(unsigned int deltaT){
    float speed = camSpeed * deltaT;

    //We apply the rotation first
    updateOrientation();

    //Then translate
    for(char x : activeMoveStates){
        switch (x){
            case 'w':
                position += front * speed;
                break;
            case '?'://shift
                speed *= 3;
                break;

            case 's':
                position -= front * speed;
                break;

            case 'a':
                position -= side * speed;
                break;

            case 'd':
                position += side * speed;
                break;

            case 'q':
                position += up * speed;
                break;

            case 'e':
                position -= up * speed;
                break;
        }
    }

    //And we recalculate the new view and projection matrices for rendering
    target = position + front;
    viewMatrix = glm::lookAt(position, target, up);
    cameraFrustum.updatePlanes(viewMatrix, position);
    projectionMatrix = glm::perspective(glm::radians(cameraFrustum.fov), cameraFrustum.AR, cameraFrustum.nearPlane, cameraFrustum.farPlane);
}

//View frustrum culling using a models AABB
bool Camera::checkVisibility(AABox *bounds){
    return cameraFrustum.checkIfInside(bounds);
}

//Used by input to reset camera to origin in case user loses their bearings
void Camera::resetCamera(){
    position = originalPosition;
    target   = originalTarget;
    front    = originalFront;
    side     = originalSide;
    pitch    = originalPitch;
    yaw      = originalYaw;
}

//Transform from cartesian to spherical, used in the first setup of yaw and pitch
//Since the incoming target and position values are being read from an unknown scene file
float Camera::getPitch(glm::vec3 front){
    return glm::degrees(glm::asin(front.y));
}
float Camera::getYaw(glm::vec3 front, float pitch){
    return glm::degrees(glm::acos(front.x / cos(glm::radians(pitch))));
}

//Orient the front and side vectors based on the screen pitch and yaw values
void Camera::updateOrientation(){
    front.x = cos(glm::radians(pitch)) * cos(glm::radians(yaw));
    front.y = sin(glm::radians(pitch));
    front.z = cos(glm::radians(pitch)) * sin(glm::radians(yaw));
    front = glm::normalize(front);
    side  = glm::normalize(glm::cross(front, up));
}