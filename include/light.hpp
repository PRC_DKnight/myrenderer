//Data structures for different types of light. CPU and GPU versions.
#pragma once
#include "glm/glm.hpp"
struct BaseLight{
    glm::vec3 color = glm::vec3(1.0f);
    glm::mat4 shadowProjectionMat = glm::mat4(0.0);

    bool  changed = false;

    float strength = 1.0f;
    float zNear    = 1.0f;
    float zFar     = 2000.0f;

    unsigned int shadowRes = 1024;
    unsigned int depthMapTextureID = 0;
};

struct DirectionalLight : public BaseLight{
    glm::vec3 direction = glm::vec3(-1.0f);

    glm::mat4 lightView = glm::mat4(0.0);
    glm::mat4 lightSpaceMatrix = glm::mat4(0.0);

    float distance;
    float orthoBoxSize;
};

struct PointLight : public BaseLight{
    glm::vec3 position = glm::vec3(0.0f);
    glm::mat4 lookAtPerFace[6];
};

//Currently only used in the generation of SSBO's for light culling and rendering
//I think it potentially would be a good idea to just have one overall light struct for all light types
//and move all light related calculations to the gpu via compute or frag shaders. This should reduce the
//number of Api calls we're currently making and also unify the current lighting path that is split between
//compute shaders and application based calculations for the matrices.
struct GPULight{
    glm::vec4 position;//0-16
    glm::vec4 color;//17-32
    unsigned int enabled;//32-60
    float intensity;//60-64
    float range;//64-68
    float padding;//68-72
};