#pragma once
#include "glm/glm.hpp"
#include "shader.hpp"
#include "texture.hpp"
#include "geometry.hpp"

#include <vector>
#include <string>

struct Vertex{
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec3 tangent;//切线
    glm::vec3 biTangent;//双相切
    glm::vec2 texCoords;
};

struct Mesh {
    //Supposedly STL is not recommended for performance, but I don't believe this is the case
    //in our current implementation
    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    std::vector<unsigned int> textures;
    Mesh(const std::vector<Vertex> &vertices,
         const std::vector<unsigned int> &indices,
         const std::vector<unsigned int> &textures)
    {
        this->vertices = vertices;
        this->indices  = indices;
        this->textures = textures;

        setupMesh();
        buildAABB();
    }
    void buildAABB();
    void setupMesh();
    void draw(const Shader &shader, bool textured,int modelId);
    AABox getAABox(){
        return aaBox;
    }
    float getArea(){
        return area;
    }
    //OpenGL drawing variables
    unsigned int VAO, VBO, EBO;
    AABox aaBox;
    float area;
};

void Mesh::buildAABB(){
    glm::vec3 minVals(std::numeric_limits<float>::max());
    glm::vec3 maxVals(std::numeric_limits<float>::min());

    //Iterating through every vertx in the mesh
    for(int i = 0; i < vertices.size(); ++i){
        //Setting max values
        maxVals.x = std::max(maxVals.x, vertices[i].position.x);
        maxVals.y = std::max(maxVals.y, vertices[i].position.y);
        maxVals.z = std::max(maxVals.z, vertices[i].position.z);

        //Setting min values
        minVals.x = std::min(minVals.x, vertices[i].position.x);
        minVals.y = std::min(minVals.y, vertices[i].position.y);
        minVals.z = std::min(minVals.z, vertices[i].position.z);
    }
    aaBox.minPoints = minVals;
    aaBox.maxPoints = maxVals;
}

//The diffuse texture is assumed to always exist and always loaded in case you want to do alpha
//discard. Lower overhead texture setup is something worth investigating here.
void Mesh::draw(const Shader &shader, bool textured,int modelId){
    //Diffuse
    glActiveTexture(GL_TEXTURE0);
    shader.setInt("albedoMap", 0);
    glBindTexture(GL_TEXTURE_2D, textures[0]);
    if(textured){
        //Emissive
        glActiveTexture(GL_TEXTURE1);
        shader.setInt("emissiveMap", 1);
        glBindTexture(GL_TEXTURE_2D, textures[1]);

        //Normals
        if (textures[2] == 0){
            shader.setBool("normalMapped", false);
        }else{
            shader.setBool("normalMapped", true);
        }
        glActiveTexture(GL_TEXTURE2);
        shader.setInt("normalsMap", 2);
        glBindTexture(GL_TEXTURE_2D, textures[2]);

        //Ambient Oclussion
        if (textures[3] == 0){
            shader.setBool("aoMapped", false);
        }else{
            shader.setBool("aoMapped", true);
        }
        glActiveTexture(GL_TEXTURE3);
        shader.setInt("lightMap", 3);
        glBindTexture(GL_TEXTURE_2D, textures[3]);

        //Metal / Roughness
        glActiveTexture(GL_TEXTURE4);
        shader.setInt("metalRoughMap", 4);
        glBindTexture(GL_TEXTURE_2D, textures[4]);

        shader.setInt("modelId",modelId);
    }

    //Mesh Drawing
    glBindVertexArray(VAO);
    glDrawElements(GL_TRIANGLES, (GLsizei)indices.size(), GL_UNSIGNED_INT, 0);
}

//Sending the data to the GPU and formatting it in memory
void Mesh::setupMesh(){
    //Generate Buffers
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    //Bind Vertex Array Object and VBO in correct order
    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);

    //VBO stuff
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

    //EBO stuff
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

    //Vertex position pointer init
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);

    //Vertex normal pointer init
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal));

    //Vertex texture coord
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, texCoords));

    //Tangent position
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, tangent));

    //Bittangent position
    glEnableVertexAttribArray(4);
    glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, biTangent));

    //Unbinding VAO
    glBindVertexArray(0);
}