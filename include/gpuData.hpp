// UBO (Uniform Buffer Object) can be used to share uniform variables
// among multiple shader programs and manage them in batches.
// Similar to SSBO (Shader Storage Buffer Object),
// this object can also be used to share data and cache data between different shaders.
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

struct VolumeTileAABB{
    glm::vec4 minPoint;
    glm::vec4 maxPoint;
} frustrum;

struct ScreenToView{
    glm::mat4 inverseProjectionMat;
    unsigned int tileSizes[4];
    unsigned int screenWidth;
    unsigned int screenHeight;
    float sliceScalingFactor;
    float sliceBiasFactor;
}screen2View;
